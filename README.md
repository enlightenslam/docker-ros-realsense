# Docker for d415/d435 using ROS

Connect d415 or d435 to your pc and enter following command in your terminal.

```
docker run --rm --net=host --privileged --volume=/dev:/dev -v <launch_file_dir>:/home/ros/launch 
-it REALSENSE_DOCKER_NAME /bin/bash
```

<launch_file_dir> is the launch  dir in this repo

To launch realsense enter the following command in your terminal:
```
roslaunch realsense2_camera launch_file.launch
```

Launching with the standard nodes:

launch docker

```
docker run --rm --net=host --privileged --volume=/dev:/dev -it REALSENSE_DOCKER_NAME /bin/bash
```

inside the docker launch the ros-launch file, you can choose any standard ros-launch file inside
the docker in `/home/ros/launch`directory
```
roslaunch realsense2_camera rs_d400_and_t265.launch
```


If you would like to change ```ROS_MASTER_URI```,

```
docker run --rm --net=host --privileged --volume=/dev:/dev -it REALSENSE_DOCKER_NAME /bin/bash -i -c 'rossetmaster TARGET_IP'
```

And launch ros-launch.